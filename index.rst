
.. index::
   pair: Django ; linkertree

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.frama.io/django/linkertree/rss.xml>`_

.. _django_linkertree:

========================
**Liens Django**
========================

- https://awesomedjango.org/
- https://gdevops.frama.io/django/versions/
- https://gdevops.frama.io/django/tuto/
- https://gdevops.frama.io/django/news/
- https://gdevops.frama.io/django/tools/


